import React from "react";
import AppBar from '@material-ui/core/AppBar';
import DevTools from "mobx-react-devtools";
import { Toolbar, Typography, IconButton, Button, withStyles } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { inject } from 'mobx-react';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },

};

// @inject('AuthStore')
class App extends React.Component {
  constructor(props) {
    super(...props);
    // move this to AuthStore
    this.state = { auth: false }
  }

  onLoginClick(event) {
    event.preventDefault();
    this.setState({ auth: true });
  }

  render() {
    return (
      <div className={this.props.classes.root}>
        {/* <DevTools />     */}
        <AppBar color="inherit">
          <Toolbar variant="dense">
            <IconButton arial-label="Open Drawer" color="primary">
              <MenuIcon />
            </IconButton>
            <Typography variant="subheading" color="primary" className={this.props.classes.grow}>
              Shopping Cart
            </Typography>
            { this.state.auth ? `Hello` : <Button color="primary" onClick={ev => this.onLoginClick(ev)}>Login</Button> }
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(App);