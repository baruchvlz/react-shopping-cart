import { observable, computed } from "mobx";


export default class ProductModel {
  @observable title = '';
  @observable price = 0.00;
  @observable category = null;
  @observable stock = 0;
  @observable inStock = false;
  @observable amountSold = 0;
  @observable totalCount = 0
  @observable createdAt = null;

  constructor(product) {
    Object.keys(product).forEach((key) => {
      this[key] = product[key];
    });
  }

  /**
   * verify if created at is within 7 days of today
   */
  @computed
  get isNew() {
    return true;
  }
}
