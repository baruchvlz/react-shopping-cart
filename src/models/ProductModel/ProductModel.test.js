import ProductModel from './ProductModel';

const mockProduct = {
  title: 'Test Product',
  price: 9.99,
  category: 'test',
  stock: 20,
  inStock: true,
  amountSold: 10,
  totalCount: 30,
  inCart: false,
  createdAt: new Date(),
};

describe('ProductModel', () => {
  it('should create a product', () => {
    const product = new ProductModel(mockProduct);

    expect(product).toBeTruthy();
  });

  it('should have function to verify if it is a new product', () => {
    const product = new ProductModel(mockProduct);

    expect(product).toHaveProperty('isNew');
  });
});